import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { MaterialModule } from './angular-material/material.module';

// Components
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AcceptDialogComponent } from './dialogs/accept-dialog/accept-dialog.component';
import { AcceptCancelDialogComponent } from './dialogs/accept-cancel-dialog/accept-cancel-dialog.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';

@NgModule({
  declarations: [
    PageNotFoundComponent,
    AcceptDialogComponent,
    AcceptCancelDialogComponent,
    BreadcrumbsComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  exports: [
    PageNotFoundComponent,
    AcceptDialogComponent,
    AcceptCancelDialogComponent,
    BreadcrumbsComponent
  ]
})
export class SharedModule { }
