import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-accept-cancel-dialog',
  templateUrl: './accept-cancel-dialog.component.html',
  styleUrls: ['./accept-cancel-dialog.component.scss']
})
export class AcceptCancelDialogComponent implements OnInit {

  constructor(
    private dialog: MatDialogRef<AcceptCancelDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

}
