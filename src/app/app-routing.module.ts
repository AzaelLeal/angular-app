import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Guards
import { AuthGuard } from '@auth/auth.guard';

// Components
import { LoginComponent } from './modules/login/login.component';
import { AppLayoutComponent } from './modules/app-layout/app-layout/app-layout.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'app', component: AppLayoutComponent, canActivate: [AuthGuard],
    loadChildren: () => import('./modules/app-layout/app-layout.module').then(m => m.AppLayoutModule)
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
