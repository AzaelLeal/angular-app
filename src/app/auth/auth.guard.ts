import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

// Services
import { AuthService } from '@auth/auth.service';
import { SnackbarService } from '@services';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService,
    private snackbarService: SnackbarService
  ) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const isLoggedUser = this.authService.isLoggedUser();
    if (isLoggedUser) {
      return true;
    } else {
      this.snackbarService.openError('You need to login');
      this.router.navigate(['login']);
    }
  }

}
