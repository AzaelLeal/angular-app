import { Injectable } from '@angular/core';
import { handleError } from './handleError';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const baseHttpOptions = {
      headers: new HttpHeaders({ customToken: 'yourToken' })
    };

    const clonedReq = request.clone(baseHttpOptions);

    return next.handle(clonedReq).pipe(catchError(handleError));
  }

}
