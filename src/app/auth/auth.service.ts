import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

// Interfaces - Services
import { IUser } from '@interfaces';
import { SnackbarService } from '@services';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private router: Router,
    private snackbarService: SnackbarService
  ) { }

  login(user: IUser) {
    const isUserLogged = this.isLoggedUser();
    if (isUserLogged) { this.removeLocalStorageUser(); }

    this.setLocalStorageUser(user);

    this.snackbarService.openSuccess('Login Successful');
    this.router.navigate(['app']);
  }

  logout() {
    const isUserLogged = this.isLoggedUser();
    if (isUserLogged) { this.removeLocalStorageUser(); }

    this.snackbarService.openSuccess('Logout Successful');
    this.router.navigate(['login']);
  }

  isLoggedUser() {
    const user = localStorage.getItem('user');
    return user ? true : false;
  }

  private removeLocalStorageUser() {
    localStorage.removeItem('user');
  }

  private setLocalStorageUser(user: IUser) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  private getLoggedUser() {
    const user = JSON.parse(localStorage.getItem('user'));
    return user;
  }

}
