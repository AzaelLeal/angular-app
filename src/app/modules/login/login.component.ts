import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// Services
import { AuthService } from '@auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {
    this.buildUserForm();
  }

  ngOnInit(): void {
  }

  private buildUserForm() {
    this.user = this.formBuilder.group({
      user: ['', [Validators.required, Validators.minLength(1)]],
      password: ['', [Validators.required, Validators.minLength(1)]]
    });
  }

  login(event: Event) {
    event.preventDefault();
    this.user.valid ? this.authService.login(this.user.value) : this.user.markAllAsTouched();
  }

  get userField() {
    return this.user.get('user');
  }

  get passwordField() {
    return this.user.get('password');
  }

}
