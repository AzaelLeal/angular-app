import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppLayoutRoutingModule } from './app-layout-routing.module';

// Components
import { HomeComponent } from '../home/home.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    AppLayoutRoutingModule,
    SharedModule
  ]
})
export class AppLayoutModule { }
